///// multiply(2)(4)(5);
function multiply(param1) {
    return function(param2) {
        return function(param3){
			return param1 * param2 * param3;
		}
    }
}
var result = multiply(2)(4)(3);
console.log(result);
/////

///// Цепочка произвольной длины с признаком конца ввода ()
function multiply(param1) {
    return function(param2) {
        if (typeof param2 !== "undefined") {
            param1 = param1 * param2;
            return multiply(param1);
        } else {
            return param1;
        }
    };
}
var result1 = multiply(2)(4)(5)(2)(5)(10)();
console.log(result1);
/////


///// generateMultiplyChain(length)
function multiply(param1) {
    var mult = param1;
    function f(param2) {
        mult = mult * param2;
        return f
    }

    f.toString = function() {
        return mult;
    }
    return f;
}

function generateMultiplyChain(length){
	for (var i=0; i<length; i++)
		return multiply;
}

var multiplyChain2 = generateMultiplyChain(2);
var result1 = multiplyChain2(4)(5)(10);
console.log(result1);
//////


//////  generateChain(length, func)
function multi(par1, par2) {
	return par1 * par2;
}

function add(par1, par2) {
	return par1 + par2;
}

function multiply(act, param1) {
    var mult = param1;
    function f(param2) {
		mult = act(param1, param2);
        return f
    }

    f.toString = function() {
        return mult;
    }
    return f;
}

function generateChain(length, act){
	for (var i=0; i<length; i++)
		return multiply(act);
}

var addChain = generateChain(3, add);

var addResult =  addChain(4)(3)(8);

console.log(addResult);

